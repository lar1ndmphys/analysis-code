void fluct_nc_sens(){

  const Int_t nL = 2; //number of detector baselines: LAr1ND @ 100m and MicroBooNE @ 470m
  Double_t EffWgt[nL] = {0.5,0.5}; //assumed reconstruction efficiency
  Double_t POTWgt[nL] = {1./3.,1.0};//= {1./3.,1.0}; //POT scaling: 1 corresponds to 6.6e20 pot. Default = {1./3.,1.};
  Int_t runnumber = 0.;

const time_t ctt = time(0);
cout << asctime(localtime(&ctt)) << endl;//output time 

  const Int_t npts = 100; //number of points on an npts x npts x npts grid of dm2, ue4, um4 (the sterile neutrino oscillation parameters)

  //Input NC pi0 event rates: one ntuple of events per detector baseline
  char file[nL][500] = {"combined_ntuple_100m_nu_processed_ncpi0_filtered.root","combined_ntuple_470m_nu_processed_ncpi0_filtered.root"};

  //Chain of events in each input ntuple
  TChain *c = new TChain("NCpi0Events");
  Float_t enugen,wgt,nuleng; //variables of interest: 
  //enugen: True neutrino energy (enters in oscillation probability)
  //wgt: Event weight, used to scale event rate up or down
  //nuleng: Neutrino travel distance
  Float_t inno; //variables used to identify event types

  //inno ID: Neutrino type ID
  Int_t nue  = 12;
  Int_t nuebar = -12;
  Int_t numu = 14;
  Int_t numubar = -14;
  //for more definitions, see https://cdcvs.fnal.gov/redmine/projects/nusoftart/repository/entry/tags/S2012-09-20/nutools/SimulationBase/MCNeutrino.h  

  //oscillated spectra (sin22th=1) as a function of baseline and dm2 
  Double_t numuEvts[nL][npts+1];
  Double_t nueEvts[nL][npts+1];

  //unoscillated spectra as a function of baseline
  Double_t numuEvtsTot[nL], variationmuon[nL];
  Double_t nueEvtsTot[nL], variationelectron[nL];
  Double_t fracsys[nL][nL]; //fractional systematics covariance matrix. The diagonals of this matrix correspond to sys**+stat**2. Off-diagonals include correlation information.
  
  Int_t ievt,ientry,nb,nbytes; //auxiliary variables for looping over event ntuples
  Int_t N=2000;//Number of samples. Higher means greater accuracy, but longer computing time
  Int_t i,j,n;

  Int_t k;

	Float_t normalisationsum[nL][nL], normalisationtobesummed[nL][nL], normalisationError[nL][nL], normalisationFracError[nL][nL];
	Float_t energysum[nL][nL], energytobesummed[nL][nL], energyError[nL][nL], energyFracError[nL][nL], FracError[nL][nL], Error[nL][nL];

for (i=0;i<nL;i++){//initialize sum as zero
	for (j=0;j<nL;j++){
	normalisationsum[i][j]=0.;
	energysum[i][j]=0.;
	}
}

  printf("\nLooping through input event ntuples...\n");

TRandom1 *r = new TRandom1(0);
TRandom1 *r1 = new TRandom1(0);
TRandom1 *r2 = new TRandom1(0);
double enugenvar;

TH1F *h1 = new TH1F("h1","h1",75,0,40000);
h1->SetTitle("Distribution of events in LAr1-ND with 20% normalisation error");
TH1F *h2 = new TH1F("h2","h2",75,0,7000);
h2->SetTitle("Distribution of events in MicroBooNE with 20% normalisation error");
TH1F *h3 = new TH1F("h3","h3",50,-10000000,10000000);
h3->SetTitle("Distribution of correlation  with 20% normalisation error");
TH1F *h4 = new TH1F("h4","h4",50,-10000000,10000000);
h4->SetTitle("Distribution of correlation");


//for (k=0;k<N;k++){//looping over the number of samples taken
  for (Int_t b=0; b<nL;b++){//Loop over the two detector baselines to calculate the number of unoscillated and Dm2-oscillated events

    nueEvtsTot[b] = 0.;//reset unoscillated rate
    numuEvtsTot[b] = 0.;//reset unoscillated rate
    variationmuon[b] = 0.;//amount of varied muon events
    variationelectron[b] = 0;//amount of varied electron events


    c->Reset();
    c->Add(file[b]);//look at input event file for baseline b
    if (c==0) return;
    const Int_t nentries = Int_t(c->GetEntries());    
    c->SetBranchAddress("enugen",&enugen);
    c->SetBranchAddress("nuleng",&nuleng);
    c->SetBranchAddress("inno",&inno);
    c->SetBranchAddress("wgt",&wgt);
Float_t varry2;

      for (ievt=0; ievt<nentries; ievt++){//Loop over all the events for baseline b
	ientry = c->LoadTree(ievt); if (ientry < 0) break; nb = c->GetEntry(ievt); nbytes += nb;

//enugenvar = r->Gaus(enugen, 0.2*enugen);//factor if varying the cross section
//calculate CV=unoscillated event rate with just ONE loop over entries, at the very beginning
	  if ((inno==numu || inno==numubar)){
	    numuEvtsTot[b] = numuEvtsTot[b] + (wgt*EffWgt[b]*POTWgt[b]);//number of mu events given no oscillation
	    //varry2 = r->Gaus(wgt*EffWgt[b]*POTWgt[b], 0.2*wgt*EffWgt[b]*POTWgt[b]);// varying each individual event by 20% error
	    //variationmuon[b] = variationmuon[b] + varry2;// varying each individual event by 20% error
	    //variationmuon[b] = variationmuon[b] + (wgt*EffWgt[b]*POTWgt[b]*(enugenvar/enugen)**2);//varying by the energy uncertainty

	  }
	  else if ((inno==nue || inno==nuebar)){
	    nueEvtsTot[b] = nueEvtsTot[b] + (wgt*EffWgt[b]*POTWgt[b]);//number of e events given no oscillation
	    //varry2 = r->Gaus(wgt*EffWgt[b]*POTWgt[b], 0.2*wgt*EffWgt[b]*POTWgt[b]);// varying each individual event by 20% error
	    //variationelectron[b] = variationelectron[b] + varry2; // varying each individual event by 20% error
	    //variationelectron[b] = variationelectron[b] + (wgt*EffWgt[b]*POTWgt[b]*(enugenvar/enugen)**2); //varying by the energy uncertainty
	  }

      }//done looping over entries
  }//done with this baseline
 


	Double_t cv[nL], normalisationvariation[nL], energyvariation[nL]; 


  Int_t L=0;

  TMatrix *covmx = new TMatrix(nL,nL);
  TMatrix *invcovmx = new TMatrix(nL,nL);



	//initialize prediction, cv
	for (b=0; b<nL; b++){
	  cv[b] = numuEvtsTot[b] + nueEvtsTot[b]; //this is the predicted event number at each detector given no oscillation
	  //variation[b] = r->Gaus(cv[b], 0.2*cv[b]);//vary the total number of events by a gaussian dist.
	  //variation[b] = variationmuon[b] + variationelectron[b];//this is used if we are varying the cross section		
	}
Double_t rando;
Int_t neg;
for (k=0;k<N;k++){//looping over the number of samples taken
		Float_t factor, energyfactor;
		factor = r->Gaus(1, 0.2);//create the Gaussian factor to weight the event rates
		normalisationvariation[0]=cv[0]*factor;//near detector
		normalisationvariation[1]=cv[1]*(2-factor);//far detector -> Negative correlation
		//normalisationvariation[1]=cv[1]*factor;//far detector -> positive correlation

		rando = r1->Rndm();
		neg = r2->Integer(2);
		if (neg==0) {rando=rando*-1;} 
		energyfactor=1. + 2*rando*0.25;
		energyvariation[0]=cv[0]*energyfactor;
		energyvariation[1]=cv[1]*energyfactor;

	for (i=0;i<nL;i++){
		for (j=0;j<nL;j++){
			normalisationtobesummed[i][j] = (cv[i]-normalisationvariation[i])*(cv[j]-normalisationvariation[j]);//create the terms to be summed
			normalisationsum[i][j] = normalisationsum[i][j]+normalisationtobesummed[i][j];//sum together the terms

			energytobesummed[i][j] = (cv[i]-energyvariation[i])*(cv[j]-energyvariation[j]);
			energysum[i][j] = energysum[i][j]+energytobesummed[i][j];
		}
	}

h1->Fill(normalisationvariation[0]);//histogram of lar1nd events
h2->Fill(normalisationvariation[1]);//histogram of microboone events
h3->Fill(normalisationtobesummed[0][1]);//histogram of off axis terms
h4->Fill(normalisationtobesummed[1][0]);
	
}//end the loop over all variations

TCanvas *c1 = new TCanvas();
h1->Draw();
TCanvas *c2 = new TCanvas();
h2->Draw();
TCanvas *c3 = new TCanvas();
h3->Draw();
TCanvas *c4 = new TCanvas();
h4->Draw();


for (i=0;i<nL;i++){
	for (j=0;j<nL;j++){
normalisationError[i][j] = normalisationsum[i][j]/N;//finish by normalising the sum. Covariance matrix
normalisationFracError[i][j] = normalisationError[i][j]/(cv[i]*cv[j]);//fractional covariance matrix

energyError[i][j] = energysum[i][j]/N;//finish by normalising the sum. Covariance matrix
energyFracError[i][j] = energyError[i][j]/(cv[i]*cv[j]);//fractional covariance matrix
	}
}
cout << "Normalisation covariance matrix:" << endl;
cout << normalisationError[0][0] << "\t" << normalisationError[0][1] << endl;
cout << normalisationError[1][0] << "\t" << normalisationError[1][1] << endl;

cout << "Normalisation Fractional covariance matrix:" << endl;
cout << normalisationFracError[0][0] << "\t" << normalisationFracError[0][1] << endl;
cout << normalisationFracError[1][0] << "\t" << normalisationFracError[1][1] << endl;
cout << " " << endl;

cout << "Energy covariance matrix:" << endl;
cout << energyError[0][0] << "\t" << energyError[0][1] << endl;
cout << energyError[1][0] << "\t" << energyError[1][1] << endl;
cout << "Energy Fractional covariance matrix:" << endl;
cout << energyFracError[0][0] << "\t" << energyFracError[0][1] << endl;
cout << energyFracError[1][0] << "\t" << energyFracError[1][1] << endl;
cout << " " << endl;

const time_t ctt2 = time(0);
cout << asctime(localtime(&ctt2)) << endl;

for (i=0;i<nL;i++){
	for (j=0;j<nL;j++){
	Error[i][j] = energyError[i][j]+normalisationError[i][j];
	FracError[i][j] = energyFracError[i][j]+normalisationFracError[i][j];
	}
}

cout << "Full  covariance matrix:" << endl;
cout << Error[0][0] << "\t" << Error[0][1] << endl;
cout << Error[1][0] << "\t" << Error[1][1] << endl;
cout << "Full Fractional covariance matrix:" << endl;
cout << FracError[0][0] << "\t" << FracError[0][1] << endl;
cout << FracError[1][0] << "\t" << FracError[1][1] << endl;
cout << " " << endl;
  TMatrix *covmx = new TMatrix(nL,nL);
  TMatrix *invcovmx = new TMatrix(nL,nL);

covmx(0,0) = FracError[0][0]*cv[0]*cv[0] + cv[0];
covmx(0,1) = FracError[0][1]*cv[0]*cv[1];
covmx(1,0) = FracError[1][0]*cv[1]*cv[0];
covmx(1,1) = FracError[1][1]*cv[1]*cv[1] + cv[1];

invcovmx = covmx->Invert();

//for (b=0; b<nL; b++){
//	  for (n=0; n<nL; n++){
//	    covmx(b,n) = (fracsys[b][n]*cv[b])*(fracsys[n][b]*cv[n]); //get the absolute systematic errors; use cv to keep errors fixed as we scan the parameter space. Multiply events by uncertainty
//	    if (b==n) covmx(b,n) = covmx(b,n)+cv[b]; //add statistical errors
//	  }
//	}

Float_t chi2;
chi2=0.;
for (b=0; b<nL; b++){//loop over baselines and add contribution
	for (n=0; n<nL; n++){
	    chi2 = chi2 + (1.2*cv[b]-cv[b])*invcovmx(b,n)*(1.2*cv[n]-cv[n]);
	}
}
cout << "chi2: " << chi2 << endl;

}
