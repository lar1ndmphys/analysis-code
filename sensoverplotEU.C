void sensoverplotEU(){

  gStyle->SetOptStat(0000);
  gStyle->SetOptFit(0000);
  gStyle->SetOptTitle(0);
  gStyle->SetPadColor(0);
  gStyle->SetCanvasColor(0);
  gStyle->SetPadBorderMode(0);
  gStyle->SetFrameBorderMode(0);
  gStyle->SetCanvasBorderMode(0);

  gStyle->SetPadBottomMargin(0.14);
  gStyle->SetPadRightMargin(0.05);
  gStyle->SetPadLeftMargin(0.14);
  gStyle->SetPadTopMargin(0.05);

  gStyle->SetTitleSize(0.05,"X");
  gStyle->SetTitleSize(0.05,"Y");

  gStyle->SetTextFont(22);
  gStyle->SetTitleFont(22,"X");
  gStyle->SetTitleFont(22,"Y");

	///Program does not sort data in text file!///
	
	const Int_t n=17; //number ofdata points in each plot
	const Int_t plotnum=3; //CHANGE TO THE NUMBER OF PLOTS WHICH ARE BEING OVERLAIN
	Int_t b;
	Int_t I;
	Float_t xvals[plotnum][n]; 
	Float_t yvals[plotnum][n];


	char detector[10];
	cout << "Have you considered the Event Uncertainty for the 'near' or 'far' detector?" << endl;
	cin >> detector;

	Int_t runnumber[plotnum];

	char typesens[5];
	char filename[plotnum][100];
	for (b=0; b<plotnum;b++){
		cout << "Please enter ms or ss and certainty level (format ms90, ms5s, etc...)" << endl;
		cin >> typesens;
		//make the file name and destination    
		snprintf(filename[b], 100, "/data/nc_sens_files/sensitivitycoords/%s_sinvsEU_%s.txt", typesens, detector);
		cout << filename[b] << endl;
	}
	
	FILE *files[plotnum];

	for (b=0; b<plotnum;b++){
		files[b] = fopen(filename[b], "rt");
		if (files == NULL)
		{
	    		printf("Error opening file!\n");
	    		exit(1);
		}
	}
	for (b=0; b<plotnum;b++){
		for (I=0; I<n;I++){
			fscanf(files[b], "%f\t%f\n", &xvals[b][I], &yvals[b][I]);
		}
	}
	for (b=0;b<plotnum;b++){
		fclose(files[b]);
	}

if (plotnum>0) TGraph *gr1 = new TGraph (n, xvals[0], yvals[0]);

if (plotnum>1) TGraph *gr2 = new TGraph (n, xvals[1], yvals[1]);

if (plotnum>2) TGraph *gr3 = new TGraph (n, xvals[2], yvals[2]);

TCanvas *c1 = new TCanvas("c1","c1",350,400);
//c1->SetLogy();

///Need to edit per plots!///
TAxis *xaxis = gr1->GetXaxis();
xaxis->SetLimits(0.0,1.5);
gr1->GetHistogram()->SetMaximum(0.56);
gr1->GetHistogram()->SetMinimum(0.0);

if (plotnum>0) {gr1->SetLineWidth(2);gr1->SetLineColor(1);gr1->Draw("AL"); TLegend *leg1 = new TLegend(0.18,0.8,0.3,1.0);leg1->SetFillStyle(0);leg1->SetBorderSize(0);leg1->SetTextSize(0.05);leg1->SetTextColor(1);}

if (plotnum>1) {gr2->SetLineWidth(2);gr2->SetLineColor(2);gr2->Draw("L"); TLegend *leg2 = new TLegend(0.18,0.75,0.3,0.95);leg2->SetFillStyle(0);leg2->SetBorderSize(0);leg2->SetTextSize(0.05);leg2->SetTextColor(2);}

if (plotnum>2) {gr3->SetLineWidth(2);gr3->SetLineColor(3);gr3->Draw("L"); TLegend *leg3 = new TLegend(0.18,0.70,0.3,0.90);leg3->SetFillStyle(0);leg3->SetBorderSize(0);leg3->SetTextSize(0.05);leg3->SetTextColor(3);}

/////Change legend every run!/////
if (plotnum>0){leg1->AddEntry("","ms90",""); leg1->Draw();}
if (plotnum>1){leg2->AddEntry("","ms3s",""); leg2->Draw();}
if (plotnum>2){leg3->AddEntry("","ms5s",""); leg3->Draw();}

char ylabel[23];
snprintf(ylabel, 23, "sin^{2}(2#theta)");
gr1->GetYaxis()->SetTitle(ylabel);
gr1->GetXaxis()->SetTitle("Event Uncertainty (1/64)");
c1->SetGrid();

char datapath[100];
snprintf(datapath, 100, "/data/nc_sens_files/%s_sinvsEU_overlay_%s.eps", typesens, detector);
 c1->Print(datapath);


}//Make number of plots flexible without changing code.
//Adjust sprintf file path *runnumber[]
	
